package com.mystique.todo.springDataDemo.repository;

import com.mystique.todo.springDataDemo.entity.Todo;
import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Todo, Long> {
}
