package com.mystique.todo.springDataDemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoSuchElementFoundException extends RuntimeException{
    private String message;

    public NoSuchElementFoundException(String msg){
        super(msg);
        this.message =msg;
        
    }


}
