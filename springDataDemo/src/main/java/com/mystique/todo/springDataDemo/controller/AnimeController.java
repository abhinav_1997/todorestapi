package com.mystique.todo.springDataDemo.controller;

import com.mystique.todo.springDataDemo.service.AnimeService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/api/v1")
@RestController
public class AnimeController {

    @Autowired
    private AnimeService animeService;

    @GetMapping("/anime")
    public ResponseEntity<Object> fetchAnime(@RequestParam(required = false) Integer id) {


        JSONObject obj = animeService.getAllAnimeFacts(id);
        return new ResponseEntity<>(obj.toMap(), HttpStatus.OK);
//        return null;
    }


}
