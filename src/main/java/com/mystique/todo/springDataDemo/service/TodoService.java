package com.mystique.todo.springDataDemo.service;

import com.mystique.todo.springDataDemo.entity.Todo;

import java.util.Map;
import java.util.Optional;

public interface TodoService
{
   Iterable<Todo> fetchAll();

   Todo adduser(Todo todo);

   Optional<Todo> fetchById(long id);

   int deleteTodoById(Long id);

   String deleteAll();

   Todo updateById(long id, Map<String, String> body);
}
