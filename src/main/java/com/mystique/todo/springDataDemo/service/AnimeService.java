package com.mystique.todo.springDataDemo.service;
import org.json.JSONObject;


public interface AnimeService {
    JSONObject getAllAnimeFacts(Integer id)  ;

}
