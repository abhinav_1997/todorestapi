package com.mystique.todo.springDataDemo.service.impl;

import com.mystique.todo.springDataDemo.entity.Todo;
import com.mystique.todo.springDataDemo.exception.NoSuchElementFoundException;
import com.mystique.todo.springDataDemo.repository.TodoRepository;
import com.mystique.todo.springDataDemo.service.TodoService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository todoRepository;


    @Override
    public Iterable<Todo> fetchAll() {
        List<Todo> list = new ArrayList<>();
        List<Todo> result;
        Iterable<Todo> iterable = todoRepository.findAll();
        Iterator<Todo> iterator = iterable.iterator();

        while (iterator.hasNext()) {
            list.add(iterator.next());
        }

        //filtering Id -Exercise 1
//        result = list.stream().filter(p -> p.getId() > 10).collect(Collectors.toList());
//        return result;

        // Exercise 2
//        result=list.stream()
//                .filter(p -> p.getTaskName().equalsIgnoreCase("Dance"))
//                .collect(Collectors.toList());
//        return result


        //Exercise 3
//        result=list.stream()
//                .filter(p -> p.getTaskId() < 100)
//                .collect(Collectors.toList());

        //Exercise4  filtering Data in descending order by Id
//        result=list.stream().sorted(Comparator.comparing(Todo::getId).reversed()).collect(Collectors.toList());
//        System.out.println(result);

        // Exercise 5  Return the minimum Id present in Todo Lists
//        Optional<Todo> t=list.stream().min(Comparator.comparing(Todo::getId));
//            return t;


//            Exercise :6
//        Map<String, Optional<Todo>> data =
//                list.stream().collect(
//                        Collectors.groupingBy(
//                                Todo::getStatus, Collectors.maxBy(Comparator.comparing(Todo::getId))
//                        )
//                );

        // Exercise : 7
//        Map<String, List<Todo>> data = list
//                .stream()
//                .collect(
//                        Collectors.groupingBy(Todo::getStatus)
//                );

//        Map<Long, Integer>  data = list
//                .stream()
//                .collect(
//                        Collectors.toMap(
//                                order -> order.getId(),
//                                order -> order.getStatus(
//                        )
//                );

            // Exercise 8
//        Date d1 = null;
//        Date d2 = null;
//        try {
//            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//            d1 = sdformat.parse("2022-09-01 00:00:00");
//            d2 = sdformat.parse("2019-09-28 00:00:00");
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//        Date finalD = d1;
//        Date finalD1 = d2;
//         result = list
//                .stream()
//                .filter(o -> o.getCreatedDate().compareTo(finalD) >= 0)
//                .filter(o -> o.getCreatedDate().compareTo(finalD1) < 0)
//                .collect(Collectors.toList());
//        System.out.println(result);

        // Exercise 9 map and flap Map

        List<Integer> l =Arrays.asList(1,2,4,5);
        List<Integer> square = l.stream().map(n -> n*n).collect(Collectors.toList());
//        System.out.println(square);

        // Flat Map
        List<Integer> list1 =Arrays.asList(1,2,4,5);
        List<Integer> list2 =Arrays.asList(6,7,8,9);
        List<Integer> list3 =Arrays.asList(11,12,13,14);

        List<List<Integer>> finalList = Arrays.asList(list1,list2,list3);
        List<Integer> output=finalList.stream().flatMap( n -> n.stream()).collect(Collectors.toList());
        System.out.println(output);



        return iterable;

    }

    @Override
    public Todo adduser(Todo todo) {

        return todoRepository.save(todo);
    }

    @Override
    public Optional<Todo> fetchById(long id) {

        return todoRepository.findById(id);

    }


    @Override
    public int deleteTodoById(Long id) {
        if (id == null)
            return 0;

        todoRepository.deleteById(id);
        return 1;


    }


    @Override
    public String deleteAll() {
        todoRepository.deleteAll();
        return "Deleted";
    }

    @Override
    public Todo updateById(long id, Map<String, String> body) throws NoSuchElementFoundException {

        Optional<Todo> optional = todoRepository.findById(id);

        try {
            if (optional.isPresent()) {
                Todo t = new Todo();
                t.setTaskName(body.get("taskName"));
                t.setStatus(body.get("status"));
                t.setId(optional.get().getId());
                t.setCreatedDate(optional.get().getCreatedDate());
                t.setLastUpdatedDate(optional.get().getLastUpdatedDate());
                t.setTaskId(optional.get().getTaskId());
                return todoRepository.save(t);
            } else {
                throw new NoSuchElementFoundException("No todo to update");
            }
        } catch (NoSuchElementFoundException e) {
            throw new RuntimeException(e);
        }

    }
}
