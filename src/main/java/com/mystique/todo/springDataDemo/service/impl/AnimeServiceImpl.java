package com.mystique.todo.springDataDemo.service.impl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Logger;
import com.mystique.todo.springDataDemo.exception.NoSuchElementFoundException;
import com.mystique.todo.springDataDemo.service.AnimeService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.List;

@Service
public class AnimeServiceImpl implements AnimeService {
    private static Logger logger = Logger.getLogger(String.valueOf(AnimeServiceImpl.class));

    RestTemplate restTemplate = new RestTemplate();

    private static final String animeAll = "https://newsapi.org/v2/top-headlines?country=us&apiKey=74e5c93991484bf4957eaeb7b282d5e5";

    @Override
    public JSONObject getAllAnimeFacts(Integer id) {


        String body = null;

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(List.of(MediaType.APPLICATION_JSON));
            HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
            HttpStatus statusCode = null;
//            ResponseEntity<String> animeResponse
                   body = restTemplate.exchange(animeAll, HttpMethod.GET, entity, String.class).getBody();
//            statusCode = b.getStatusCode();
//            body = animeResponse.getBody();
            System.out.println(body);


            JSONObject jsonObject = new JSONObject(body);
            JSONArray data = jsonObject.getJSONArray("articles");
            System.out.println(data);

            if (id != null) {

                if(id <= 0 || id > 13) {
                    throw new NoSuchElementFoundException("No item found with requested Id");

                }
                JSONObject idData = data.getJSONObject(id - 1);
                return idData;
            }
            else {

                List<JSONObject> list = new ArrayList<>();
                JSONArray sortedJsonArray = new JSONArray();
                for (int i = 0; i < data.length(); i++) {

                    list.add(data.getJSONObject(i));
                }

                //  replace above for loop with foreach loop
                // change the if -else
                // replace the below code with lambda and streams



                Collections.sort(list, new Comparator<JSONObject>() {

                    public int compare(JSONObject a, JSONObject b) {
                        String valA = null;
                        String valB = null;

                        try {
                            valA =  (String) a.get("title");
                            valB =  (String) b.get("title");
                        } catch (JSONException e) {

                        }

                        return valA.compareTo(valB);
                    }
                });

                for (int i = 0; i < data.length(); i++) {
                    sortedJsonArray.put(list.get(i));
                }

                JSONObject obj = new JSONObject();
                obj.put("Data", sortedJsonArray);
                System.out.println(sortedJsonArray);



//                if (statusCode == HttpStatus.OK) {
//                    System.out.println("success");
//
//                } else
//                    System.out.println("not success");


                return obj;
            }


        } catch (Exception e) {

            logger.info("Unexpected error" + e);
            logger.warning("Unexpected error" + e);
        }
        return null;
    }
}
