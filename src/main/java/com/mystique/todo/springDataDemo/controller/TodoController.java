package com.mystique.todo.springDataDemo.controller;

import com.mystique.todo.springDataDemo.entity.Todo;
import com.mystique.todo.springDataDemo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class TodoController {

    // Fetching from application.yml
    @Value("${user.role}")
    private String userRole;

    @GetMapping("/")
    public String home(){
        return userRole;

    }

    @Autowired
    private TodoService todoservice;

    // get all Todo's - read operation
    @GetMapping("/todo")
    public ResponseEntity<Iterable<Todo>> fetchAll() {
        return ResponseEntity.status(HttpStatus.OK).body(todoservice.fetchAll());

    }

    // get Todo By Id - read operation
    @GetMapping("todo/{id}")
    public ResponseEntity<Optional<Todo>> fetchById(@PathVariable long id) {
        Optional<Todo> optional = todoservice.fetchById(id);
        if (optional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(optional);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    // Create todo List - Create operation
    @PostMapping("/save")
    public Todo createUser(@RequestBody Todo todo) {
        return todoservice.adduser(todo);
    }

    //Delete By Id - Delete operation
    @DeleteMapping("/todo/{id}")
    public ResponseEntity<String> deleteDepartmentById(@PathVariable Long id) {
        if (todoservice.deleteTodoById(id) == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }
        return ResponseEntity.status(HttpStatus.OK).body("deleted successfully");


    }

    // Delete all
    @DeleteMapping("/deleteAll")
    public ResponseEntity<String> deleteDepartmentById() {
        try {
            String status = todoservice.deleteAll();
            if (status == "deleted")
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return null;
    }


    // Update operation
    @PutMapping("/todo/{id}")
    public ResponseEntity<Todo> update(@PathVariable long id, @RequestBody Map<String, String> body) {

        Todo todo = todoservice.updateById(id, body);
        if (Objects.nonNull(todo))
            return ResponseEntity.status(HttpStatus.OK).body(todo);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);


    }


}
