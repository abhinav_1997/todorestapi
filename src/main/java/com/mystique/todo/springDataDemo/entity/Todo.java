package com.mystique.todo.springDataDemo.entity;

import lombok.Data;


import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@Table(name = "todo")
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name="taskId")
    private Long taskId;

    @Column(name = "taskName")
    private String taskName;

    @Column(name = "createdDate")
    private Date createdDate;

    @Column(name = "lastUpdatedDate")
    private Date lastUpdatedDate;

    @Column(name = "status")
    private String status;


}
